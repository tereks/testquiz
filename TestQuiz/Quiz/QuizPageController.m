//
//  QuizPageController.m
//  TestQuiz
//
//  Created by Sergey Kim on 13.04.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "QuizPageController.h"
#import "RButton.h"

#import "Meaning.h"
#include <stdlib.h>
#import "UIColor+Extra.h"

#import "QuizPageDetailController.h"

NSString * const detailSegueIdentifier = @"showDetail";

@interface QuizPageController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet RButton *button1;
@property (weak, nonatomic) IBOutlet RButton *button2;
@property (weak, nonatomic) IBOutlet RButton *button3;
@property (weak, nonatomic) IBOutlet RButton *button4;
@property (weak, nonatomic) IBOutlet RButton *skipButton;

@property (strong, nonatomic) IBOutletCollection(RButton) NSArray *answerButtons;

@end

@implementation QuizPageController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self loadDataFromQuestion];
}

- (void) setupUI {
    for ( RButton * button in self.answerButtons ) {
        [button addBorderWithWidth:1 andColor:[UIColor choiceButtonBorderColor]];
    }
}

- (void) loadDataFromQuestion {
    if ( self.question ) {
        self.titleLabel.text = self.question.text;
        
        [self shuffleAnswers];
    }
}

- (void) shuffleAnswers {
    
    unsigned int rightAnswerIndex = arc4random_uniform( (unsigned int)self.answerButtons.count );
    RButton * rightButton = self.answerButtons[rightAnswerIndex];
    [rightButton setTitle:_question.translation forState:UIControlStateNormal];
    
    NSMutableArray * alternatives = [NSMutableArray arrayWithArray:[_question.alternatives copy]];
    for (NSUInteger i = 0; i < self.answerButtons.count; i++) {
        if ( i == rightAnswerIndex ) {
            continue;
        }
        unsigned int index = arc4random_uniform( (unsigned int)alternatives.count );
        Alternative * alternative = alternatives[index];
        
        RButton * button = self.answerButtons[i];
        [button setTitle:alternative.translation forState:UIControlStateNormal];
        
        [alternatives removeObjectAtIndex:index];
    }
}

- (IBAction)answerSelected:(RButton *)sender {
    if ( [sender.titleLabel.text isEqualToString:_question.translation] ) {
        [sender setBackgroundColor:[UIColor choiceButtonRightColor]];
        [self setQuestionState:YES];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self showDetailViewController];
        });
    }
    else {
        [sender setBackgroundColor:[UIColor choiceButtonWrongColor]];
        [self setQuestionState:NO];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self selectRightAnswer];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self showDetailViewController];
            });
        });        
    }
}

- (void) selectRightAnswer {
    for ( RButton * button in self.answerButtons ) {
        if ( [button.titleLabel.text isEqualToString:_question.translation] ) {
            [button setBackgroundColor:[UIColor choiceButtonRightColor]];
            break;
        }
    }
}

- (void) setQuestionState:(BOOL)rightAnswer {
    self.question.answeredRight = @(rightAnswer);
}

- (IBAction)skipButtonSelected:(id)sender {
    [self selectRightAnswer];
    [self setQuestionState:NO];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self showDetailViewController];
    });
}

- (void) showDetailViewController {
    [self performSegueWithIdentifier:detailSegueIdentifier sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:detailSegueIdentifier]) {
        QuizPageDetailController * detailController = segue.destinationViewController;
        detailController.question = self.question;
        detailController.delegate = self.delegate;
    }
}

@end
