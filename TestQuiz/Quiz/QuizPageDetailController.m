//
//  QuizPageDetailController.m
//  TestQuiz
//
//  Created by Sergey Kim on 13.04.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "QuizPageDetailController.h"
#import "Meaning.h"

#import "UIImageView+AFNetworking.h"

@interface QuizPageDetailController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *itemImageView;
@property (weak, nonatomic) IBOutlet UILabel *translationLabel;

@end

@implementation QuizPageDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadDataFromQuestion];
}

- (void) loadDataFromQuestion {
    if ( self.question ) {
        self.titleLabel.text = self.question.text;
        self.translationLabel.text = self.question.translation;
        
        NSString *imageString = self.question.images ? self.question.images[0] : nil;
        imageString = [NSString stringWithFormat:@"http:%@", imageString];
        [self.itemImageView setImageWithURL:[NSURL URLWithString:imageString]];
    }
}

- (IBAction)nextButtonSelected:(id)sender {
    if ( [_delegate respondsToSelector:@selector(moveToNextQuestion)] ) {
        [_delegate moveToNextQuestion];
    }
}

@end
