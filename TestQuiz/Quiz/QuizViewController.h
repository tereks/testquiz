//
//  QuizViewController.h
//  TestQuiz
//
//  Created by Sergey Kim on 13.04.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Meaning.h"

@interface QuizViewController : UIViewController

@property (nonatomic, readonly, strong) NSArray<Meaning*> * quizQuestions;

@end
