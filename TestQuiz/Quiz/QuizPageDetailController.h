//
//  QuizPageDetailController.h
//  TestQuiz
//
//  Created by Sergey Kim on 13.04.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuizPageController.h"
@class Meaning;

@interface QuizPageDetailController : UIViewController

@property (nonatomic, strong) Meaning* question;

@property (nonatomic, weak) id<QuizPageControllerDelegate> delegate;

@end
