//
//  QuizViewController.m
//  TestQuiz
//
//  Created by Sergey Kim on 13.04.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "QuizViewController.h"
#import "UIColor+Extra.h"

#import <YLProgressBar/YLProgressBar.h>
#import <SVProgressHUD/SVProgressHUD.h>

#import "QuizAPI.h"
#import "QuizManager.h"
#import "QuizPageController.h"
#import "ScoreViewController.h"

NSString * const scoreSegueIdentifier = @"showScore";

@interface QuizViewController () <QuizPageControllerDelegate>

@property (nonatomic, weak) IBOutlet YLProgressBar * progressBar;
@property (nonatomic, weak) UIPageViewController * pageViewController;
@property (nonatomic, readwrite, strong) NSArray * quizQuestions;
@property (nonatomic, assign) NSUInteger currentIndex;

@end

@implementation QuizViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _currentIndex = 0;
    [self setupUI];   
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self loadQuestions]; 
}

- (void) setupUI {
    _progressBar.type                     = YLProgressBarTypeFlat;
    _progressBar.hideGloss                = YES;
    _progressBar.hideStripes              = YES;
    _progressBar.indicatorTextDisplayMode = YLProgressBarIndicatorTextDisplayModeNone;
    _progressBar.progressTintColors       = @[[UIColor progressTintColor], [UIColor progressTintColor]];
    _progressBar.trackTintColor           = [UIColor clearColor];
    _progressBar.behavior                 = YLProgressBarBehaviorDefault;
    
    _progressBar.layer.cornerRadius       = CGRectGetHeight(_progressBar.bounds) / 2.f;
    _progressBar.clipsToBounds            = YES;
    _progressBar.layer.borderColor        = [UIColor progressTintColor].CGColor;
    _progressBar.layer.borderWidth        = 1.f;
    
    _progressBar.progress = 0;
}

- (void) loadQuestions {
    [SVProgressHUD show];
    
    [QuizAPI 
     loadWordsForIdentifiers:[QuizManager instance].wordIdentifiers 
     screenWidth:CGRectGetWidth([[UIScreen mainScreen] bounds]) 
     completion:^(NSArray<Meaning *> *meanings, NSError *error) {
         if ( !error ) {
             self.quizQuestions = meanings;
             dispatch_async(dispatch_get_main_queue(), ^{
                 [SVProgressHUD dismiss];
                 
                 [self.progressBar setProgress:0];
                 [self openPageWithIndex:_currentIndex];
             });
         }
     }];    
}

- (UINavigationController *)viewControllerWithIndex:(NSUInteger)index {
    if (([self.quizQuestions count] == 0) || (index >= [self.quizQuestions count])) {
        return nil;
    }
    
    UINavigationController * navController = [self.storyboard instantiateViewControllerWithIdentifier:@"QuizPageController"];
    QuizPageController *pageContentViewController = (QuizPageController*)navController.topViewController;
    pageContentViewController.delegate = self;
    pageContentViewController.question = self.quizQuestions[index];
    
    return navController;
}

- (void) openPageWithIndex:(NSUInteger)index {
    UINavigationController * navController = [self viewControllerWithIndex:index];
    
    NSArray *viewControllers = @[navController];
    [self.pageViewController setViewControllers:viewControllers 
                                      direction:UIPageViewControllerNavigationDirectionForward 
                                       animated:YES 
                                     completion:nil];
}

- (BOOL) prefersStatusBarHidden {
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"PageEmbed"]) {
        self.pageViewController = segue.destinationViewController;
    }
    if ([segue.identifier isEqualToString:scoreSegueIdentifier]) {
        ScoreViewController * scoreController = segue.destinationViewController;
        scoreController.questions = self.quizQuestions;
    }
}

#pragma mark - QuizPageControllerDelegate

- (void) moveToNextQuestion {
    _currentIndex += 1;
    
    if ( _currentIndex < [QuizManager instance].wordIdentifiers.count ) {
        CGFloat progress = (_currentIndex) * 1.0 / (double)[QuizManager instance].wordIdentifiers.count;
        [self.progressBar setProgress:progress];
        
        [self openPageWithIndex:_currentIndex];
    }
    else {
        [self performSegueWithIdentifier:scoreSegueIdentifier sender:self];
    }
}

@end
