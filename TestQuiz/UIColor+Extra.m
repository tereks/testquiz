//
//  UIColor+Extra.m
//  TestQuiz
//
//  Created by Sergey Kim on 13.04.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "UIColor+Extra.h"

@implementation UIColor (Extra)

+ (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

+ (UIColor*) progressTintColor {
    return [UIColor colorFromHexString:@"#DDDDDD"];
}

+ (UIColor*) choiceButtonBorderColor {
    return [UIColor colorFromHexString:@"#CCCCCC"];
}

+ (UIColor*) choiceButtonRightColor {
    return [UIColor colorFromHexString:@"#ADD166"];
}

+ (UIColor*) choiceButtonWrongColor {
    return [UIColor colorFromHexString:@"#FF8080"];
}

@end
