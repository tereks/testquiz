//
//  QuizManager.m
//  TestQuiz
//
//  Created by Sergey Kim on 13.04.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "QuizManager.h"
#include <stdlib.h>

NSUInteger const testWordsCount = 10;

@interface QuizManager ()

@property (nonatomic, readwrite, strong) NSArray * initialIdentifiers;
@property (nonatomic, readwrite, strong) NSArray * wordIdentifiers;

@end

@implementation QuizManager

+ (instancetype) instance {
    static QuizManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[self alloc] init];
    });
    return manager;
}

- (id) init {
    if (self = [super init]) {
        self.initialIdentifiers = @[ @"211138",
                                     @"226138",
                                     @"177344",
                                     @"196957",
                                     @"224324",
                                     @"89785",
                                     @"79639",
                                     @"173148",
                                     @"136709",
                                     @"158582",
                                     @"92590",
                                     @"135793",
                                     @"68068",
                                     @"64441",
                                     @"46290",
                                     @"55112",
                                     @"51254",
                                     @"222435",
                                     @"128173"];
    }
    return self;
}

- (void) setupTestWords {
    NSMutableArray * initialIdentifiersCopy = [NSMutableArray arrayWithArray:[self.initialIdentifiers copy]];
    NSMutableArray * randomArray = [NSMutableArray array];
    
    NSUInteger count = testWordsCount < initialIdentifiersCopy.count ? testWordsCount : initialIdentifiersCopy.count;
    for (NSUInteger i = 0; i < count; i++) {
        unsigned int index = arc4random_uniform( (unsigned int)initialIdentifiersCopy.count );
        
        [randomArray addObject:initialIdentifiersCopy[index]];
        [initialIdentifiersCopy removeObjectAtIndex:index];
    }
    
    self.wordIdentifiers = [[NSArray alloc] initWithArray:randomArray];
}

@end
