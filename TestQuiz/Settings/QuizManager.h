//
//  QuizManager.h
//  TestQuiz
//
//  Created by Sergey Kim on 13.04.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QuizManager : NSObject

+ (instancetype) instance;

@property (nonatomic, readonly, strong) NSArray * wordIdentifiers;

- (void) setupTestWords;

@end
