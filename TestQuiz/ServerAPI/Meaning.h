//
//  Meaning.h
//  TestQuiz
//
//  Created by Sergey Kim on 13.04.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "JSONModel.h"

@protocol Alternative
@end

@interface Alternative : JSONModel

@property (nonatomic, strong) NSString* translation;
@property (nonatomic, strong) NSString* text;

@end

@interface Meaning : JSONModel

@property (nonatomic, strong) NSNumber* meaningId;
@property (nonatomic, strong) NSString* posCode;
@property (nonatomic, strong) NSString* text;
@property (nonatomic, strong) NSString* translation;
@property (nonatomic, strong) NSString* definition;
@property (nonatomic, strong) NSString* transcription;
@property (nonatomic, strong) NSString* soundUrl;
@property (nonatomic, strong) NSArray * images;
@property (nonatomic, strong) NSArray<Optional, Alternative>* alternatives;

@property (nonatomic, strong) NSNumber<Optional> *answeredRight;

@end
