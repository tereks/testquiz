//
//  QuizAPI.m
//  TestQuiz
//
//  Created by Sergey Kim on 13.04.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "QuizAPI.h"
#import "AFNetworking.h"
#import "Meaning.h"

NSString *const APIendpoint = @"http://dictionary.skyeng.ru/api/v1";

@implementation QuizAPI

+ (void) loadWordsForIdentifiers:(NSArray*)identifiers
                     screenWidth:(double)width
                      completion:(MeaningsCompletion)completion {
    
    NSString * urlString = [NSString stringWithFormat:@"%@/wordtasks", APIendpoint];
    
    NSDictionary * params = @{ @"width" : @(width),
                               @"meaningIds" : [identifiers componentsJoinedByString:@","] };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:urlString 
      parameters:params 
        progress:nil 
         success:^(NSURLSessionTask *task, id responseObject) {
             NSError * error;
             
             NSArray * meanings = [Meaning arrayOfModelsFromDictionaries:responseObject error:&error];
             if ( !error ) {
                 completion( meanings, nil );
             }
             else {
                 completion( nil, error );
             }
         } failure:^(NSURLSessionTask *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             
             completion( nil, error );
         }];
}

@end
