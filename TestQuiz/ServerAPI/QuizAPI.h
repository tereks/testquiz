//
//  QuizAPI.h
//  TestQuiz
//
//  Created by Sergey Kim on 13.04.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Meaning;

typedef void (^MeaningsCompletion)( NSArray<Meaning*> *meanings, NSError* error );

@interface QuizAPI : NSObject

+ (void) loadWordsForIdentifiers:(NSArray*)identifiers
                      screenWidth:(double)width
                      completion:(MeaningsCompletion)completion;

@end
