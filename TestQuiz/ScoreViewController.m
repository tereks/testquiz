//
//  ScoreViewController.m
//  TestQuiz
//
//  Created by Sergey Kim on 13.04.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "ScoreViewController.h"
#import "Meaning.h"

@interface ScoreViewController ()

@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;

@end

@implementation ScoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ( self.questions.count > 0 ) {
        NSUInteger rightAnswersCount = 0;
        for ( Meaning* question in self.questions ) {
            if ( question.answeredRight.boolValue ) {
                rightAnswersCount += 1;
            }
        }
        self.scoreLabel.text = [NSString stringWithFormat:@"%ld/%ld", rightAnswersCount, self.questions.count ];
    }
}

- (IBAction)againButtonSelected:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
