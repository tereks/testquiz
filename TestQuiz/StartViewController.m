//
//  StartViewController.m
//  TestQuiz
//
//  Created by Sergey Kim on 13.04.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "StartViewController.h"
#import "QuizManager.h"

NSString * const gotoQuizSegueIdentifier = @"gotoQuizSegueIdentifier";

@interface StartViewController ()

@end

@implementation StartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)startButtonSelected:(id)sender {
    [self performSegueWithIdentifier:gotoQuizSegueIdentifier sender:self];
}


#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:gotoQuizSegueIdentifier]) {
        [[QuizManager instance] setupTestWords];
    }
}


@end
