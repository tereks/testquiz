//
//  UIColor+Extra.h
//  TestQuiz
//
//  Created by Sergey Kim on 13.04.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Extra)

+ (UIColor *)colorFromHexString:(NSString *)hexString;

+ (UIColor*) progressTintColor;
+ (UIColor*) choiceButtonBorderColor;

+ (UIColor*) choiceButtonRightColor;
+ (UIColor*) choiceButtonWrongColor;

@end
