//
//  RButton.m
//  TestQuiz
//
//  Created by Sergey Kim on 13.04.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "RButton.h"
#import <QuartzCore/QuartzCore.h>

@implementation RButton

- (void) awakeFromNib {
    self.layer.cornerRadius = CGRectGetHeight(self.bounds) / 2.f;
    self.clipsToBounds = YES;
}

- (void) addBorderWithWidth:(CGFloat)width andColor:(UIColor*)borderColor {
    self.layer.borderWidth = width;
    self.layer.borderColor = borderColor.CGColor;
}

@end
