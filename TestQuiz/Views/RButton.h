//
//  RButton.h
//  TestQuiz
//
//  Created by Sergey Kim on 13.04.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RButton : UIButton

- (void) addBorderWithWidth:(CGFloat)width andColor:(UIColor*)borderColor;

@end
