//
//  ScoreViewController.h
//  TestQuiz
//
//  Created by Sergey Kim on 13.04.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Meaning;

@interface ScoreViewController : UIViewController

@property (strong, nonatomic) NSArray<Meaning*> *questions;

@end
